## Résumé

(Une phrase décrivant le problème.)

## Comment reproduire

(Étapes à suivre pour observer le comportement erroné.)

## Comportement erroné actuel

## Résultat correct attendu

## Quel appareil/environnement

(Si applicable, sur quel appareils/navigateurs web est-ce que ce problème est réplicable.)

/label ~"type::bug"
/cc @Fab1en
