<?php

namespace Tests\Integration;

use App\Events\RegistrationSubmittedEvent;
use App\Models\Community;
use App\Models\File;
use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Illuminate\Testing\Assert;
use Mockery;
use Noke;
use Stripe;
use Tests\TestCase;

class UserTest extends TestCase
{
    private static $userResponseStructure = [
        "id",
        "name",
        "email",
        "email_verified_at",
        "description",
        "date_of_birth",
        "address",
        "postal_code",
        "phone",
        "is_smart_phone",
        "other_phone",
    ];

    private static $getCommunityResponseStructure = [
        "id",
        "name",
        "description",
        "area",
        "created_at",
        "updated_at",
        "deleted_at",
        "type",
        "center",
    ];

    public function testOrderUsersById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", "/api/v1/communities/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderUsersByFullName()
    {
        $data = [
            "order" => "full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderUsersByEmail()
    {
        $data = [
            "order" => "email",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersById()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "id" => "3",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByCreatedAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "@",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByFullName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "full_name" => "Ariane",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByIsDeactivated()
    {
        // Zero integer
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deactivated" => 0,
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        $notDeactivatedResponseContent = $response->baseResponse->getContent();

        // Positive integer
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deactivated" => 1,
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        $deactivatedResponseContent = $response->baseResponse->getContent();

        // Boolean false
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deactivated" => false,
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        Assert::assertJsonStringEqualsJsonString(
            $notDeactivatedResponseContent,
            $response->baseResponse->getContent()
        );

        // Boolean true
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deactivated" => true,
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        Assert::assertJsonStringEqualsJsonString(
            $deactivatedResponseContent,
            $response->baseResponse->getContent()
        );
    }

    public function testFilterUsersByCommunityId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "communities.id" => "3",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByCommunityName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "communities.name" => "Patrie",
        ];
        $response = $this->json("GET", "/api/v1/users/", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testCreateUsers()
    {
        $data = [
            "accept_conditions" => true,
            "gdpr" => true,
            "newsletter" => true,
            "name" => $this->faker->name,
            "last_name" => $this->faker->name,
            "email" => $this->faker->unique()->safeEmail,
            "password" =>
                '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            "description" => null,
            "date_of_birth" => null,
            "address" => "",
            "postal_code" => "",
            "phone" => "",
            "is_smart_phone" => false,
            "other_phone" => "",
            "remember_token" => Str::random(10),
        ];

        $response = $this->json("POST", "/api/v1/users", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                "id",
                "name",
                "last_name",
                "email",
                "description",
                "date_of_birth",
                "address",
                "postal_code",
                "phone",
                "is_smart_phone",
                "other_phone",
                "accept_conditions",
                "gdpr",
                "newsletter",
                "created_at",
                "updated_at",
            ]);
    }

    public function testCreateUsersWithSimilarEmails()
    {
        $user = factory(User::class)
            ->make()
            ->toArray();
        $user["password"] = "12354123124";

        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(201);

        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(422);

        // Case insensitivity
        $user["email"] =
            strtoupper($user["email"][0]) . substr($user["email"], 1);
        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(422);
    }

    public function testShowUsers()
    {
        $user = factory(User::class)->create();
        $data = [];

        $response = $this->json("GET", "/api/v1/users/$user->id", $data);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "id",
                "name",
                "last_name",
                "email",
                "email_verified_at",
                "description",
                "date_of_birth",
                "address",
                "postal_code",
                "phone",
                "is_smart_phone",
                "other_phone",
                "remember_token",
                "created_at",
                "updated_at",
                "role",
                "full_name",
            ]);
    }

    public function testUpdateUsers()
    {
        $user = factory(User::class)->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testUpdateUsersWithNonexistentId()
    {
        $user = factory(User::class)->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/users/208084082084", $data);

        $response->assertStatus(404);
    }

    public function testDeleteUsers()
    {
        $user = factory(User::class)->create();

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(200);
    }

    public function testDeleteUsersWithNonexistentId()
    {
        $user = factory(User::class)->create();

        $response = $this->json("DELETE", "/api/v1/users/0280398420384");

        $response->assertStatus(404);
    }

    public function testListUsers()
    {
        $users = factory(User::class, 2)
            ->create()
            ->map(function ($user) {
                return $user->only(static::$userResponseStructure);
            });

        $response = $this->json("GET", "/api/v1/users");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->buildCollectionStructure(static::$userResponseStructure)
            );
    }

    public function testAssociateUserToCommunity()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();

        $response = $this->json(
            "PUT",
            "/api/v1/users/$user->id/communities/$community->id"
        );
        $response->assertStatus(200);
    }

    public function testUpdateUserWithCommunity()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();

        $data = [
            "communities" => [["id" => $community->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
    }

    public function testUpdateUserWithProofForNewCommunity_fails()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();
        $proof = factory(File::class)->create([
            "field" => "proof",
        ]);

        $data = [
            "communities" => [
                ["id" => $community->id, "proof" => [["id" => $proof->id]]],
            ],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response->assertStatus(422);
    }

    public function testUpdateUserWithCommunityProof_triggersRegistrationSubmitted()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();
        $user->communities()->save($community);

        $proof = factory(File::class)->create([
            "field" => "proof",
        ]);

        $data = [
            "communities" => [
                ["id" => $community->id, "proof" => [["id" => $proof->id]]],
            ],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithIdenticCommunityProof_doesntTriggerRegistrationSubmitted()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();
        $user->communities()->save($community);

        $proof = factory(File::class)->create([
            "field" => "proof",
        ]);
        $user->communities[0]->pivot->proof()->save($proof);

        $data = [
            "communities" => [
                ["id" => $community->id, "proof" => [["id" => $proof->id]]],
            ],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithoutCommunityProof_doesntTriggerRegistrationSubmitted()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();
        $user->communities()->save($community);

        $data = [
            "communities" => [["id" => $community->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);
    }

    public function testShowUsersCommunities()
    {
        $user = factory(User::class)->create();
        $community = factory(Community::class)->create();
        $otherCommunity = factory(Community::class)->create();

        $data = [
            "communities" => [["id" => $community->id]],
        ];
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response->assertStatus(200);

        $data = [
            "users.id" => $user->id,
        ];

        $response = $this->json(
            "GET",
            "/api/v1/communities/$otherCommunity->id",
            $data
        );
        $response->assertStatus(404);

        $response = $this->json(
            "GET",
            "/api/v1/communities/$community->id",
            $data
        );
        $response->assertStatus(200);
    }

    public function testListUsersCommunities()
    {
        $user = factory(User::class)->create();
        $communities = factory(Community::class, 2)->create();

        $data = [
            "communities" => [["id" => $communities[0]->id]],
        ];
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);

        $data = [
            "users.id" => $user->id,
        ];
        $response = $this->json("GET", "/api/v1/communities", $data);
        $response
            ->assertStatus(200)
            ->assertJson(["total" => 1])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getCommunityResponseStructure
                )
            );
    }

    public function testUsersUpdateEmailEndpointUpdatesNokeEmail()
    {
        $originalEmail = "test@gmail.com";
        $user = factory(User::class)->create([
            "email" => $originalEmail,
            "role" => "admin",
        ]);

        $newEmail = "different@hotmail.com";

        Noke::shouldReceive("findUserByEmail")
            ->withArgs(function ($a) use ($originalEmail) {
                return $a === $originalEmail;
            })
            ->andReturns(
                (object) [
                    "username" => $originalEmail,
                ]
            )
            ->once();

        Noke::shouldReceive("updateUser")
            ->withArgs(function ($arg) use ($newEmail) {
                return $arg->username === $newEmail;
            })
            ->once();

        $this->json("POST", "/api/v1/users/$user->id/email", [
            "email" => $newEmail,
        ])->assertStatus(200);
    }

    public function testUsersUpdateEndpointUpdatesNokeEmail()
    {
        $originalEmail = "test@gmail.com";
        $user = factory(User::class)->create([
            "email" => $originalEmail,
        ]);

        $newEmail = "different@hotmail.com";

        Noke::shouldReceive("findUserByEmail")
            ->withArgs(function ($a) use ($originalEmail) {
                return $a === $originalEmail;
            })
            ->andReturns(
                (object) [
                    "username" => $originalEmail,
                ]
            )
            ->once();

        Noke::shouldReceive("updateUser")
            ->withArgs(function ($arg) use ($newEmail) {
                return $arg->username === $newEmail;
            })
            ->once();

        $this->json(
            "PUT",
            "/api/v1/users/$user->id",
            array_merge($user->toArray(), [
                "email" => $newEmail,
            ])
        )->assertStatus(200);
    }

    public function testAddToBalanceEndpoint()
    {
        $paymentMethod = factory(PaymentMethod::class)->create([
            "user_id" => $this->user->id,
            "type" => "credit_card",
            "external_id" => "stripe source id",
        ]);

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->withArgs(function ($arg) {
                return $arg->id === $this->user->id;
            })
            ->andReturn((object) ["id" => "cus_test"]);

        Stripe::shouldReceive("computeAmountWithFee")
            ->once()
            ->with(10, Mockery::any())
            ->andReturn(10.5);

        Stripe::shouldReceive("createCharge")
            ->once()
            ->with(
                1050,
                "cus_test",
                "Ajout au compte LocoMotion: 10,00$ + 0,50$ (frais)",
                "stripe source id"
            );

        $response = $this->json("PUT", "/api/v1/auth/user/balance", [
            "transaction_id" => 1,
            "amount" => 10,
            "payment_method_id" => $paymentMethod->id,
        ]);
        $response->assertStatus(200)->assertSee("10");
    }
}
