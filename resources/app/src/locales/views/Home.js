export default {
  fr: {
    neighborhood: "aucun voisinage | un voisinage | {count} voisinages",
    borough: "aucun quartier | un quartier | {count} quartiers",
  },
};
