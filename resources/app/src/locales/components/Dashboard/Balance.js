export default {
  fr: {
    approvisionner: "Approvisionner",
    reclamer: "Réclamer",
    reclamer_tooltip: "Un minimum de 10$ est requis pour réclamer son solde.",
    approvisionner_popover:
      "Approvisionnez votre compte pour économiser sur les frais de transaction.",
  },
};
