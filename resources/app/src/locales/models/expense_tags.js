export default {
  fr: {
    fields: {
      id: "ID",
      name: "nom",
      slug: "nom système",
      color: "couleur",
      admin: "Seulement visible pour l'admin dans la liste proposée"
    },
    list: {
      create: "ajouter un type",
      selected: "{count} type sélectionné | {count} types sélectionnés",
    },
    model_name: "type de dépense | types de dépenses",
  },
};
