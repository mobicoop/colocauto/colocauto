import Loan from "@/views/Loan.vue";

export default [
  {
    path: "/loans/:id",
    name: "single-loan",
    component: Loan,
    props: true,
    meta: {
      auth: true,
      slug: "loans",
      skipCleanup(to) {
        return to.name === "community-view";
      },
      data: {
        loans: {
          options: {},
        },
      },
      params: {
        fields: [
          "*",
          "total_estimated_cost",
          "actions.*",
          "borrower.id",
          "borrower.user.avatar",
          "borrower.user.name",
          "borrower.user.last_name",
          "borrower.user.full_name",
          "borrower.user.phone",
          "extensions.*",
          "handover.*",
          "handover.image.*",
          "handover.expense.*",
          "incidents.*",
          "intention.*",
          "loanable.name",
          "loanable.community.name",
          "loanable.owner.user.avatar",
          "loanable.owner.user.name",
          "loanable.owner.user.last_name",
          "loanable.owner.user.full_name",
          "loanable.owner.user.phone",
          "loanable.padlock.name",
          "loanable.position",
          "loanable.type",
          "loanable.comments",
          "loanable.image",
          "loanable.instructions",
          "loanable.has_padlock",
          "loanable.location_description",
          "loanable.is_self_service",
          "bike.model",
          "bike.bike_type",
          "bike.size",
          "car.brand",
          "car.model",
          "car.year_of_circulation",
          "car.transmission_mode",
          "car.engine",
          "car.papers_location",
          "trailer.maximum_charge",
          "trailer.dimensions",
          "payment.*",
          "pre_payment.*",
          "takeover.*",
          "takeover.image.*",
          "expenses.id",
          "expenses.amount",
          "expenses.tag",
        ].join(","),
      },
    },
  },
];
