@extends('emails.layouts.main_text')

@section('content')
Bonjour!

Vous recevez ce courriel car vous avez récemment débuté votre inscription sur le site
Coloc'Auto. Ceci est un petit rappel pour finaliser votre profil.

Une fois l'inscription complétée vous allez recevoir un courriel avec la marche à suivre
pour participer au programme.

N'hésitez pas à communiquer avec nous pour toute question!

L'équipe Coloc'Auto
soutien@colocauto.org
@endsection
