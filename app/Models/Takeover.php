<?php

namespace App\Models;

use Carbon\Carbon;

class Takeover extends Action
{
    public static $rules = [
        "status" => "required",
        "mileage_beginning" => ["nullable"],
        "comments_on_vehicle" => ["nullable"],
    ];

    public static $sizes = [
        "thumbnail" => "256x@fit",
    ];

    public static function getColumnsDefinition()
    {
        return [
            "*" => function ($query = null) {
                if (!$query) {
                    return "takeovers.*";
                }

                return $query->selectRaw("takeovers.*");
            },
            "type" => function ($query = null) {
                if (!$query) {
                    return "'takeover' AS type";
                }

                return $query->selectRaw("'takeover' AS type");
            },
        ];
    }

    protected $fillable = ["mileage_beginning", "comments_on_vehicle"];

    public $readOnly = false;

    public $morphOnes = [
        "image" => "imageable",
    ];

    public function image()
    {
        return $this->morphOne(Image::class, "imageable");
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function complete($at = null)
    {
        $this->executed_at = new Carbon($at);
        $this->status = "completed";

        return $this;
    }

    public function isCompleted()
    {
        return $this->status == "completed";
    }

    public function contest($at = null)
    {
        // Status = canceled means contested.
        $this->executed_at = new Carbon($at);
        $this->status = "canceled";

        return $this;
    }

    public function isContested()
    {
        return $this->status == "canceled";
    }
}
