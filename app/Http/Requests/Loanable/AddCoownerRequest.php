<?php

namespace App\Http\Requests\Loanable;

use App\Http\Requests\BaseRequest;
use App\Models\Loanable;
use App\Models\User;

class AddCoownerRequest extends BaseRequest
{
    public function authorize(): bool
    {
        $user = $this->user();
        if (!$user) {
            return false;
        }
        $loanable = Loanable::find($this->route("loanable_id"));

        if( $user->isAdmin() ){
            return true;
        }

        if( $user->isCommunityAdmin() ){
            if( Loanable::accessibleBy($user)->find($this->route("loanable_id")) ){
                return true;
            }
        }

        return $user->is($loanable->owner->user);
    }

    public function rules(): array
    {
        /** @var User $user */
        $user = $this->user();

        if ($user->isAdmin() || $user->isCommunityAdmin()) {
            $authorizedUsers = User::accessibleBy($this->user())
                ->pluck("id")
                ->join(",");
        } else {
            $authorizedUsers = User::whereHas(
                "approvedCommunities",
                function ($q) use ($user) {
                    $q->withApprovedUser($user);
                }
            )
                ->pluck("id")
                ->join(",");
        }

        return [
            "user_id" => ["required", "in:$authorizedUsers"],
        ];
    }
}
