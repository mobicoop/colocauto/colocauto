<?php

namespace App\Http\Requests\Refund;

use App\Http\Requests\BaseRequest;
use App\Models\User;

class CreateRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            "amount" => [
                "numeric",
                "required",
                "gte:0"
            ],
            "user_id" => [
                "numeric",
                "required",
            ],
            "credited_user_id" => [
                "numeric",
                "required",
                "different:user_id"
            ]
        ];

        if( !$this->user()->isAdmin() ) {
            $user = $this->user();
            $accessibleUserIds = implode(
                ",",
                User::accessibleBy($user)
                    ->pluck("id")
                    ->toArray()
            );
            $rules["user_id"][] = "in:$accessibleUserIds";
            $rules["credited_user_id"][] = "in:$accessibleUserIds";
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            "user_id" => "payé par",
            "credited_user_id" => "payé à",
            "amount" => "montant"
        ];
    }

    public function messages()
    {
        return [
            "credited_user_id.required" => "Vous devez indiquer à qui vous avez remboursé une somme.",
            "credited_user_id.different" => "La personne qui paye et celle qui est remboursée doivent être différentes",
        ];
    }
}
